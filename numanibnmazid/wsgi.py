
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'numanibnmazid.settings.development')
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'numanibnmazid.settings.production')

application = get_wsgi_application()
