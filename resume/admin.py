from django.contrib import admin

from util.mixings import CustomAdminMixing

from .models import (
    WorkArea, TechnicalSkill, TrainingSummary, Project, ProjectAttachment, ResourceLink,
    EmploymentHistory, AcademicQualification, PersonalInformation, Address,
    LanguageProficiency, SoftSkill, ExtraCurricularActivity, Award, Reference,
    InternetOfThing, Contact
)


class WorkAreaAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = WorkArea
    

class TechnicalSkillAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = TechnicalSkill


class TrainingSummaryAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = TrainingSummary


class ProjectAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = Project


class ProjectAttachmentAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = ProjectAttachment


class ResourceLinkAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = ResourceLink


class EmploymentHistoryAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = EmploymentHistory


class AcademicQualificationAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = AcademicQualification


class PersonalInformationAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = PersonalInformation


class AddressAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = Address


class LanguageProficiencyAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = LanguageProficiency


class SoftSkillAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = SoftSkill


class ExtraCurricularActivityAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = ExtraCurricularActivity


class AwardAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = Award


class ReferenceAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = Reference


class InternetOfThingAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = InternetOfThing


class ContactAdmin(CustomAdminMixing, admin.ModelAdmin):
    pass

    class Meta:
        model = Contact



admin.site.register(WorkArea, WorkAreaAdmin)
admin.site.register(TechnicalSkill, TechnicalSkillAdmin)
admin.site.register(TrainingSummary, TrainingSummaryAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectAttachment, ProjectAttachmentAdmin)
admin.site.register(ResourceLink, ResourceLinkAdmin)
admin.site.register(EmploymentHistory, EmploymentHistoryAdmin)
admin.site.register(AcademicQualification, AcademicQualificationAdmin)
admin.site.register(PersonalInformation, PersonalInformationAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(LanguageProficiency, LanguageProficiencyAdmin)
admin.site.register(SoftSkill, SoftSkillAdmin)
admin.site.register(ExtraCurricularActivity, ExtraCurricularActivityAdmin)
admin.site.register(Award, AwardAdmin)
admin.site.register(Reference, ReferenceAdmin)
admin.site.register(InternetOfThing, InternetOfThingAdmin)
admin.site.register(Contact, ContactAdmin)

