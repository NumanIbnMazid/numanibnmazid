# Generated by Django 3.0.3 on 2020-02-24 21:11

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('resume', '0011_auto_20200224_0548'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='softskill',
            name='skill',
        ),
        migrations.AddField(
            model_name='softskill',
            name='skill_name',
            field=models.CharField(default='Default', max_length=200, verbose_name='Skill Name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='technicalskill',
            name='skill_name',
            field=models.CharField(default=django.utils.timezone.now, max_length=200, verbose_name='Skill Name'),
            preserve_default=False,
        ),
    ]
