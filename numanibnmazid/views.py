from django.views.generic import TemplateView
from django.shortcuts import render
from resume.models import (
    WorkArea, TechnicalSkill, TrainingSummary, Project, ProjectAttachment, ResourceLink,
    EmploymentHistory, AcademicQualification, PersonalInformation, Address,
    LanguageProficiency, SoftSkill, ExtraCurricularActivity, Award, Reference,
    InternetOfThing, Contact
)

class HomeView(TemplateView):
    # template_name = "base.html"
    def get(self, request, *args, **kwargs):
        # Work Area
        work_areas = WorkArea.objects.all()
        # TechnicalSkill
        technical_skills = TechnicalSkill.objects.all()
        # TrainingSummary
        training_summaries = TrainingSummary.objects.all()
        # Project
        projects = Project.objects.all()
        # ProjectAttachment
        project_attachments = ProjectAttachment.objects.all()
        # ResourceLink
        resource_links = ResourceLink.objects.all()
        # EmploymentHistory
        employement_histories = EmploymentHistory.objects.all()
        # AcademicQualification
        academic_qualifications = AcademicQualification.objects.all()
        # PersonalInformation
        personal_informations = PersonalInformation.objects.all()
        # Address
        addresses = Address.objects.all()
        # LanguageProficiency
        language_proficiencies = LanguageProficiency.objects.all()
        # SoftSkill
        soft_skills = SoftSkill.objects.all()
        # ExtraCurricularActivity
        extra_curricular_activities = ExtraCurricularActivity.objects.all()
        # Award
        awards = Award.objects.all()
        # Reference
        references = Reference.objects.all()
        # InternetOfThing
        internet_of_things = InternetOfThing.objects.all()
        # Contact
        contacts = Contact.objects.all()
        # Passing Context
        context = {
            'work_areas': work_areas,
            'technical_skills': technical_skills,
            'training_summaries': training_summaries,
            'projects': projects,
            'project_attachments': project_attachments,
            'resource_links': resource_links,
            'employement_histories': employement_histories,
            'academic_qualifications': academic_qualifications,
            'personal_informations': personal_informations,
            'addresses': addresses,
            'language_proficiencies': language_proficiencies,
            'soft_skills': soft_skills,
            'extra_curricular_activities': extra_curricular_activities,
            'awards': awards,
            'references': references,
            'internet_of_things': internet_of_things,
            'contacts': contacts,
            'icon_list' : [
                'hackerrank', 'youtube', 'coderbyte', 'codewars', 'stackoverflow', 
                'twitter', 'github', 'bitbucket', 'linkedin', 'facebook',
                'python', 'php', 'asp.net', 'quasar', 'reactjs', 'vuejs',
                'c#', 'javascript', 'jquery', 'laravel', 'website', 'html'
            ]
        }
        return render(request, 'pages/home.html', context=context)


class AdminDashboardView(TemplateView):
    template_name = "dashboard/pages/portion/dashboard.html"
