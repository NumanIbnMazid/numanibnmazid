from django.db import models
from .choices import (TECHNICAL_SKILL_CHOICES, LEVEL_CHOICES,
                      STATUS_CHOICES, GENDER_CHOICES,
                      BLOOD_GROUP_CHOICES, ADDRESS_CATEGORY_CHOICES,
                      PROFICIENCY_CHOICES, DIFFICULTY_LEVEL_CHOICES,
                      JOB_TYPE_CHOICES
                      )
from .utils import upload_project_files_path, upload_profile_image_path


class WorkArea(models.Model):
    work_area = models.CharField(
        max_length=100, verbose_name="Work Area"
    )
    designation = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Designation"
    )
    career_objective = models.TextField(
        blank=True, null=True, verbose_name="Career Objective", max_length=1000
    )
    preffered_technical_work_area = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Preferred Work Area"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = 'Work Area'
        verbose_name_plural = 'Work Areas'
        ordering = ['-updated_at']

    def __str__(self):
        return self.work_area


class TechnicalSkill(models.Model):
    skill_category = models.CharField(
        max_length=100, choices=TECHNICAL_SKILL_CHOICES, verbose_name='Skill Category'
    )
    skill_name = models.CharField(
        max_length=200, verbose_name='Skill Name'
    )
    skill_level = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=LEVEL_CHOICES, verbose_name="Skill Level"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Technical Skill"
        verbose_name_plural = "Technical Skills"
        ordering = ['-updated_at']

    def __str__(self):
        return self.skill_category


class TrainingSummary(models.Model):
    certificate_name = models.CharField(
        max_length=200, verbose_name="Certificate Name"
    )
    institute = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Institute"
    )
    country = models.CharField(
        blank=True, null=True, max_length=100, verbose_name='Country'
    )
    location = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='Location'
    )
    certificate_no = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Certificate No."
    )
    from_date = models.DateField(
        blank=True, null=True, verbose_name="From"
    )
    to_date = models.DateField(
        blank=True, null=True, verbose_name="To"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Training Summary"
        verbose_name_plural = "Training Summaries"
        ordering = ['-updated_at']

    def __str__(self):
        return self.certificate_name


class Project(models.Model):
    title = models.CharField(
        max_length=255, verbose_name="Project Title"
    )
    slug = models.SlugField(
        max_length=255, unique=True, verbose_name='Slug'
    )
    technology = models.TextField(
        blank=True, null=True, max_length=500, verbose_name="Technology Used"
    )
    link = models.URLField(
        blank=True, null=True, verbose_name="Project Link"
    )
    source_code_link = models.URLField(
        blank=True, null=True, verbose_name="Source Code Link"
    )
    status = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES, default=0, verbose_name='Project Status'
    )
    short_description = models.TextField(
        blank=True, null=True, verbose_name="Short Description"
    )
    features = models.TextField(
        blank=True, null=True, verbose_name='Features'
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"
        ordering = ['-updated_at']

    def __str__(self):
        return self.title


class ProjectAttachment(models.Model):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name='project_attachments', verbose_name='Project'
    )
    slug = models.SlugField(
        max_length=255, unique=True, verbose_name='slug'
    )
    title = models.CharField(
        max_length=100, verbose_name='title', blank=True, null=True
    )
    file = models.FileField(
        upload_to=upload_project_files_path, blank=True, null=True, verbose_name='attachments'
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='created at'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='updated at'
    )

    class Meta:
        verbose_name = "Project Attachment"
        verbose_name_plural = "Project Attachments"
        ordering = ['-updated_at']

    def __str__(self):
        return self.project.title


class ResourceLink(models.Model):
    title = models.CharField(
        max_length=100, verbose_name="Resource Title"
    )
    link = models.URLField(
        blank=True, null=True, verbose_name="Resource Link"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='created at'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='updated at'
    )

    class Meta:
        verbose_name = 'Resource Link'
        verbose_name_plural = 'Resource Links'
        ordering = ['-updated_at']

    def __str__(self):
        return self.title


class EmploymentHistory(models.Model):
    organization = models.CharField(
        max_length=100, verbose_name="Organization"
    )
    job_type = models.CharField(
        max_length=100, choices=JOB_TYPE_CHOICES, default='Part Time', verbose_name='Job Type'
    )
    work_position = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Work Position"
    )
    from_date = models.DateField(
        blank=True, null=True, verbose_name="From Date"
    )
    to_date = models.DateField(
        blank=True, null=True, verbose_name="To Date"
    )
    currently_working = models.BooleanField(
        default=False, verbose_name="Currently Working"
    )
    location = models.CharField(
        max_length=255, blank=True, null=True, verbose_name="Location"
    )
    country = models.CharField(
        blank=True, null=True, max_length=100, verbose_name="Country"
    )
    responsibility = models.TextField(
        max_length=1000, blank=True, null=True, verbose_name="Responsibility"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='created at'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='updated at'
    )

    class Meta:
        verbose_name = 'Employment History'
        verbose_name_plural = 'Employment Histories'
        ordering = ['-updated_at']

    def __str__(self):
        return self.organization


class AcademicQualification(models.Model):
    title = models.CharField(
        max_length=100, verbose_name="Exam Title"
    )
    major = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Major"
    )
    institute = models.CharField(
        max_length=100, verbose_name="Institute"
    )
    pass_year = models.CharField(
        max_length=10, verbose_name="Pass Year"
    )
    duration = models.CharField(
        max_length=100, verbose_name="Duration"
    )
    achievement = models.CharField(
        max_length=100, verbose_name="Achievement"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='created at'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='updated at'
    )

    class Meta:
        verbose_name = 'Academic Qualification'
        verbose_name_plural = 'Academic Qualifications'
        ordering = ['-updated_at']

    def __str__(self):
        return self.title


class PersonalInformation(models.Model):
    full_name = models.CharField(
        max_length=100, verbose_name="Full Name"
    )
    nick_name = models.CharField(
        max_length=50, verbose_name="Nick Name"
    )
    image = models.ImageField(
        upload_to=upload_profile_image_path, null=True, blank=True, verbose_name='image'
    )
    father_name = models.CharField(
        max_length=100, verbose_name="Father Name"
    )
    father_occupation = models.CharField(
        max_length=100, verbose_name="Father's Ocupation", blank=True, null=True
    )
    mother_name = models.CharField(
        max_length=100, verbose_name="Mother Name"
    )
    mother_occupation = models.CharField(
        max_length=100, verbose_name="Mother's Occupation", blank=True, null=True
    )
    date_of_birth = models.DateField(
        verbose_name="Date of Birth"
    )
    gender = models.CharField(
        max_length=100, choices=GENDER_CHOICES, verbose_name="Gender"
    )
    full_name = models.CharField(
        max_length=100, verbose_name="Full Name"
    )
    marital_status = models.CharField(
        max_length=100, verbose_name="Marital Status"
    )
    nationality = models.CharField(
        max_length=50, verbose_name="Nationality"
    )
    religion = models.CharField(
        max_length=50, verbose_name="Religion"
    )
    height = models.CharField(
        max_length=50, verbose_name="Height", blank=True, null=True
    )
    weight = models.CharField(
        max_length=50, verbose_name="Weight", blank=True, null=True
    )
    blood_group = models.CharField(
        max_length=20, choices=BLOOD_GROUP_CHOICES, verbose_name="Blood Group"
    )
    nid_no = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="National ID Card No"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='created at'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='updated at'
    )

    class Meta:
        verbose_name = 'Personal Information'
        verbose_name_plural = 'Personal Informations'
        ordering = ['-updated_at']

    def __str__(self):
        return self.full_name


class Address(models.Model):
    address_type = models.CharField(
        max_length=100, choices=ADDRESS_CATEGORY_CHOICES, verbose_name="Address Type"
    )
    village = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Village"
    )
    post_office = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Post Office"
    )
    thana = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Thana"
    )
    address = models.CharField(
        max_length=255, blank=True, null=True, verbose_name="Address"
    )
    district = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="District"
    )
    division = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Division"
    )
    postal_code = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Postal Code"
    )
    country = models.CharField(
        max_length=100, blank=True, null=True, verbose_name="Country"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='created at'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='updated at'
    )

    class Meta:
        verbose_name = 'Address'
        verbose_name_plural = 'Addresses'
        ordering = ['-updated_at']

    def __str__(self):
        return self.address_type


class LanguageProficiency(models.Model):
    language = models.CharField(
        max_length=100, verbose_name="Language"
    )
    proficiency = models.CharField(
        max_length=100, choices=PROFICIENCY_CHOICES, verbose_name='Proficiency'
    )
    listening = models.CharField(
        max_length=50, choices=DIFFICULTY_LEVEL_CHOICES, verbose_name='Listening'
    )
    speaking = models.CharField(
        max_length=50, choices=DIFFICULTY_LEVEL_CHOICES, verbose_name='Speaking'
    )
    reading = models.CharField(
        max_length=50, choices=DIFFICULTY_LEVEL_CHOICES, verbose_name='Reading'
    )
    writing = models.CharField(
        max_length=50, choices=DIFFICULTY_LEVEL_CHOICES, verbose_name='writing'
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='created at'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='updated at'
    )

    class Meta:
        verbose_name = 'Language Proficiency'
        verbose_name_plural = 'Language Proficiencies'
        ordering = ['-updated_at']

    def __str__(self):
        return self.language


class SoftSkill(models.Model):
    skill_name = models.CharField(
        max_length=200, verbose_name='Skill Name'
    )
    skill_level = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=LEVEL_CHOICES, verbose_name="Skill Level"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Soft Skill"
        verbose_name_plural = "Soft Skills"
        ordering = ['-updated_at']

    def __str__(self):
        return self.skill


class ExtraCurricularActivity(models.Model):
    activity = models.CharField(
        max_length=100, verbose_name='Activity'
    )
    skill_level = models.PositiveSmallIntegerField(
        blank=True, null=True, choices=LEVEL_CHOICES, verbose_name="Skill Level"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Extra Curricular Activity"
        verbose_name_plural = "Extra Curricular Activities"
        ordering = ['-updated_at']

    def __str__(self):
        return self.activity


class Award(models.Model):
    title = models.CharField(
        max_length=255, verbose_name='Title'
    )
    achievement = models.CharField(
        max_length=255, verbose_name='Achievement', blank=True, null=True
    )
    institute = models.CharField(
        max_length=255, verbose_name='Institute', blank=True, null=True
    )
    country = models.CharField(
        max_length=50, verbose_name='Country', blank=True, null=True
    )
    location = models.CharField(
        max_length=255, verbose_name='Location', blank=True, null=True
    )
    date = models.DateField(
        verbose_name='Date', blank=True, null=True
    )
    certificate_no = models.CharField(
        max_length=255, verbose_name='Certificate No.', blank=True, null=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Award"
        verbose_name_plural = "Awards"
        ordering = ['-updated_at']

    def __str__(self):
        return self.title


class Reference(models.Model):
    name = models.CharField(
        max_length=100, verbose_name='Name'
    )
    information = models.TextField(
        max_length=1000, blank=True, null=True, verbose_name='Information'
    )
    contact = models.CharField(
        max_length=50, blank=True, null=True, verbose_name='Contact'
    )
    email = models.CharField(
        max_length=100, blank=True, null=True, verbose_name='Email'
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Reference"
        verbose_name_plural = "References"
        ordering = ['-updated_at']

    def __str__(self):
        return self.name


class InternetOfThing(models.Model):
    title = models.CharField(
        max_length=100, verbose_name='Title'
    )
    url = models.URLField(
        verbose_name='URL'
    )
    add_to_intro = models.BooleanField(
        default=False, verbose_name="Add to Intro"
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Internet Of Thing"
        verbose_name_plural = "Internet Of Things"
        ordering = ['-updated_at']

    def __str__(self):
        return self.title


class Contact(models.Model):
    contact = models.CharField(
        max_length=50, verbose_name='Contact'
    )
    contact_2 = models.CharField(
        max_length=50, verbose_name='Contact 2', blank=True, null=True
    )
    contact_3 = models.CharField(
        max_length=50, verbose_name='Contact 3', blank=True, null=True
    )
    email = models.EmailField(
        verbose_name='Email', blank=True, null=True
    )
    email_2 = models.EmailField(
        verbose_name='Email 2', blank=True, null=True
    )
    email_3 = models.EmailField(
        verbose_name='Email 3', blank=True, null=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created At'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Updated At'
    )

    class Meta:
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"
        ordering = ['-updated_at']

    def __str__(self):
        return self.contact
