# Generated by Django 3.0.3 on 2020-02-25 01:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resume', '0013_auto_20200225_0153'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='academicqualification',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Academic Qualification', 'verbose_name_plural': 'Academic Qualifications'},
        ),
        migrations.AlterModelOptions(
            name='address',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Address', 'verbose_name_plural': 'Addresses'},
        ),
        migrations.AlterModelOptions(
            name='award',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Award', 'verbose_name_plural': 'Awards'},
        ),
        migrations.AlterModelOptions(
            name='contact',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Contact', 'verbose_name_plural': 'Contacts'},
        ),
        migrations.AlterModelOptions(
            name='employmenthistory',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Employment History', 'verbose_name_plural': 'Employment Histories'},
        ),
        migrations.AlterModelOptions(
            name='extracurricularactivity',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Extra Curricular Activity', 'verbose_name_plural': 'Extra Curricular Activities'},
        ),
        migrations.AlterModelOptions(
            name='languageproficiency',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Language Proficiency', 'verbose_name_plural': 'Language Proficiencies'},
        ),
        migrations.AlterModelOptions(
            name='personalinformation',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Personal Information', 'verbose_name_plural': 'Personal Informations'},
        ),
        migrations.AlterModelOptions(
            name='project',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Project', 'verbose_name_plural': 'Projects'},
        ),
        migrations.AlterModelOptions(
            name='projectattachment',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Project Attachment', 'verbose_name_plural': 'Project Attachments'},
        ),
        migrations.AlterModelOptions(
            name='reference',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Reference', 'verbose_name_plural': 'References'},
        ),
        migrations.AlterModelOptions(
            name='resourcelink',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Resource Link', 'verbose_name_plural': 'Resource Links'},
        ),
        migrations.AlterModelOptions(
            name='softskill',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Soft Skill', 'verbose_name_plural': 'Soft Skills'},
        ),
        migrations.AlterModelOptions(
            name='technicalskill',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Technical Skill', 'verbose_name_plural': 'Technical Skills'},
        ),
        migrations.AlterModelOptions(
            name='trainingsummary',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Training Summary', 'verbose_name_plural': 'Training Summaries'},
        ),
        migrations.AlterModelOptions(
            name='workarea',
            options={'ordering': ['-updated_at'], 'verbose_name': 'Work Area', 'verbose_name_plural': 'Work Areas'},
        ),
    ]
