import os
# import random
# import string
import time
# from django.utils.text import slugify


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_profile_image_path(instance, filename):
    new_filename = "image_{datetime}".format(
        datetime=time.strftime("%Y%m%d-%H%M%S")
    )
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(
        new_filename=new_filename, ext=ext)
    return "Profile-Picture/{final_filename}".format(
        final_filename=final_filename
    )


def upload_project_files_path(instance, filename):
    new_filename = "{title}_{datetime}".format(
        title=instance.project.title[:20],
        datetime=time.strftime("%Y%m%d-%H%M%S")
    )
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(
        new_filename=new_filename, ext=ext)
    return "Projects/{title}/{final_filename}".format(
        title=instance.project.title[:20],
        final_filename=final_filename
    )
