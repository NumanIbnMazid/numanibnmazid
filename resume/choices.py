# Technical Skill Choices
PROGRAMMING_LANGUAGE = 'Programming Language'
FRAMEWORK = 'Framework'
WEB_TECHNOLOGY = 'Web Technology'
DATABASE = 'Database'
OPERATING_SYSTEM = 'Operating System'
SOFTWARE = 'Software'
OTHER = 'Other'
TECHNICAL_SKILL_CHOICES = (
    (PROGRAMMING_LANGUAGE, 'Programming Language'),
    (FRAMEWORK, 'Framework'),
    (WEB_TECHNOLOGY, 'Web Technology'),
    (DATABASE, 'Database'),
    (OPERATING_SYSTEM, 'Operating System'),
    (SOFTWARE, 'Software'),
    (OTHER, 'Other'),
)

# Level Choices
ONE = 1
TWO = 2
THREE = 3
FOUR = 4
FIVE = 5
SIX = 6
SEVEN = 7
EIGHT = 8
NINE = 9
TEN = 10
LEVEL_CHOICES = (
    (ONE, 1),
    (TWO, 2),
    (THREE, 3),
    (FOUR, 4),
    (FIVE, 5),
    (SIX, 6),
    (SEVEN, 7),
    (EIGHT, 8),
    (NINE, 9),
    (TEN, 10),
)

# Status Choices
ACTIVE = 0
INACTIVE = 1
STATUS_CHOICES = (
    (ACTIVE, 'Active'),
    (INACTIVE, 'Inactive'),
)

# Gender Choices
MALE = "M"
FEMALE = "F"
OTHER = "O"
GENDER_CHOICES = (
    (MALE, "Male"),
    (FEMALE, "Female"),
    (OTHER, "Other"),
)

# Blood Group Choices
A_POSITIVE = 'A+'
A_NEGATIVE = 'A-'
B_POSITIVE = 'B+'
B_NEGATIVE = 'B-'
O_POSITIVE = 'O+'
O_NEGATIVE = 'O-'
AB_POSITIVE = 'AB+'
AB_NEGATIVE = 'AB-'
BLOOD_GROUP_CHOICES = (
    (A_POSITIVE, 'A+'),
    (A_NEGATIVE, 'A-'),
    (B_POSITIVE, 'B+'),
    (B_NEGATIVE, 'B-'),
    (O_POSITIVE, 'O+'),
    (O_NEGATIVE, 'O-'),
    (AB_POSITIVE, 'AB+'),
    (AB_NEGATIVE, 'AB-'),
)

# Address Category Choices
PERMANENT = "Permanent"
PRESENT = "Present"
ADDRESS_CATEGORY_CHOICES = (
    (PERMANENT, "Permanent"),
    (PRESENT, "Present"),
)

# Proficiency Choioces
BASIC_KNOWLEDGE = 'Basic Knowledge'
CONVERSANT = 'Conversant'
PROFICIENT = 'Proficient'
FLUENT = 'Fluent'
NATIVE = 'Native'
BILINGUAl = 'Bilingual'
PROFICIENCY_CHOICES = (
    (BASIC_KNOWLEDGE, 'Basic Knowledge'),
    (CONVERSANT, 'Conversant'),
    (PROFICIENT, 'Proficient'),
    (FLUENT, 'Fluent'),
    (NATIVE, 'Native'),
    (BILINGUAl, 'Bilingual'),
)

# Difficulty Level Choices
NOVICE = 'Novice'
INTERMIDIATE = 'Interidiate'
ADVANCED = 'Advanced'
SUPERIOR = 'Superior'
DIFFICULTY_LEVEL_CHOICES = (
    (NOVICE, 'Novice'),
    (INTERMIDIATE, 'Interidiate'),
    (ADVANCED, 'Advanced'),
    (SUPERIOR, 'Superior'),
)

# Job Type Choices
PART_TIME = 'Part Time'
FULL_TIME = 'Full Time'
JOB_TYPE_CHOICES = (
    (PART_TIME, 'Part Time'),
    (FULL_TIME, 'Full Time'),
)
